import pandas as pd
import matplotlib.pyplot as plt

total= pd.read_csv("data.csv", header=0, sep=",")

total.plot(x ='Price', y='Units_Sold', kind='line')
plt.ylim(ymin=0)
plt.xlim(xmin=0)

plt.show()

import pandas as pd
import numpy as np

total = pd.read_csv("data.csv", header=0, sep=",")

x = total["Price"]
y = total["Units_Sold"]
slope_intercept = np.polyfit(x,y,1)

print(slope_intercept)
